#!/usr/bin/env python
import json
from collections import defaultdict

import os_client_config

hosts = defaultdict(lambda: defaultdict(list))
hosts["_meta"] = {'hostvars': {}}


def main():
    nova = os_client_config.make_client('compute', cloud='vagrant')
    for server in nova.servers.list():

        # There lots uncertainty and assumption in ths block
        for interface in server.addresses.values()[0]:
            if interface['OS-EXT-IPS:type'] == 'fixed':
                ip = interface['addr']

        power_state = server.to_dict()['OS-EXT-STS:power_state']
        power_state_group = 'power_state_{}'.format(power_state)

        vm_state = server.to_dict()['OS-EXT-STS:vm_state']
        vm_state_group = 'vm_state_{}'.format(vm_state)

        host = server.to_dict()['OS-EXT-SRV-ATTR:host']
        host_group = 'host_{}'.format(host)

        flavor = nova.flavors.get(server.flavor['id']).name
        flavor_group = 'flavor_{}'.format(flavor)

        hosts[power_state_group]['hosts'].append(ip)
        hosts[vm_state_group]['hosts'].append(ip)
        hosts[host_group]['hosts'].append(ip)
        hosts[flavor_group]['hosts'].append(ip)
        hosts['all']['hosts'].append(ip)
        hosts['_meta']['hostvars'][ip] = {
            'power_state': power_state,
            'vm_state': vm_state,
            'host': host,
            'flavor': flavor
        }

    print(json.dumps(hosts))

main()
